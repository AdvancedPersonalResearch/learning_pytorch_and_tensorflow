# Collection of various Deep learning architectures programmed with Pytorch and Tensorflow

This repository consists of the re-implementation of popular deep convolutional neural networks architectures I have programmed with Tensorflow and Pytorch.

## Getting start

 001-AlexNet architecture

 002-VGG16 architecture

 003-VGG19 architecture

 004-GoogleNet (Inception v1) architecture

 005-MobileNet_architecture v1

 006-Xception_architecture
 

 
